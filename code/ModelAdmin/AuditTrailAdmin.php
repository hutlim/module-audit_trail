<?php

class AuditTrailAdmin extends GeneralModelAdmin {
    private static $url_segment = 'audit-trail';
    private static $menu_title = 'Audit Trail';
    private static $menu_icon = 'audit_trail/images/audit-icon.png';
	private static $managed_models = array(
		'AuditTrail'
	);
}
