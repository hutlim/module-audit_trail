<?php
class AuditTrail extends DataObject{
	private static $db = array(
		'ObjectClass' => 'Varchar(250)',
		'RowID' => 'Int',
		'Version' => 'Int'
	);
	
	private static $has_many = array(
		'AuditTrailDetails' => 'AuditTrailDetail'
	);
	
	private static $default_sort = "Created DESC, ID DESC";
    
    private static $searchable_fields = array(
    	'LastEdited' => array(
			'field' => 'DateField',
			'filter' => 'DateMatchFilter'
		),
    	'ObjectClass',
    	'RowID',
        'Version' => array(
            'filter' => 'GreaterThanOrEqualFilter'
        )
    );

    private static $summary_fields = array(
        'LastEdited.Nice',
        'ObjectClass',
        'RowID',
        'Version',
        'UpdatedBy',
        'UpdatedIP'
    );
	
	public function fieldLabels($includerelations = true) {
		$labels = parent::fieldLabels($includerelations);
		
		$labels['LastEdited'] = _t('AuditTrail.LAST_EDITED', 'Last Edited');
		$labels['LastEdited.Nice'] = _t('AuditTrail.LAST_EDITED', 'Last Edited');
		$labels['ObjectClass'] = _t('AuditTrail.OBJECT_CLASS', 'Object Class');
		$labels['RowID'] = _t('AuditTrail.ROW_ID', 'Row ID');
		$labels['Version'] = _t('AuditTrail.VERSION', 'Version');
		$labels['AuditTrailDetails'] = _t('AuditTrail.AUDIT_TRAIL_DETAILS', 'Audit Trail Details');
		$labels['UpdatedBy'] = _t('AuditTrail.UPDATED_BY', 'Updated By');
		$labels['UpdatedIP'] = _t('AuditTrail.UPDATED_IP', 'Updated IP');
		
		return $labels;	
	}

	function getDefaultSearchContext() {
		$search_context = parent::getDefaultSearchContext();
		$search_context->getSearchFields()->replaceField('ObjectClass', DropdownField::create('ObjectClass', $this->fieldLabel('ObjectClass'), AuditTrail::get()->distinct('ObjectClass')->map('ObjectClass', 'ObjectClass')->toArray())->setEmptyString(_t('AuditTrail.ALL', 'All')));
		return $search_context;
  	}
	
	function getCMSFields() {
        $fields = parent::getCMSFields();

        if($this->exists()){
        	$fields->makeFieldReadonly('ObjectClass');
			$fields->makeFieldReadonly('RowID');
			$fields->makeFieldReadonly('Version');
			
            if(ClassInfo::exists('GridFieldExportToExcelButton')){
	        	$exportButton = new GridFieldExportToExcelButton('buttons-after-left');
			}
			else{
				$exportButton = new GridFieldExportButton('buttons-after-left');
			}
	        $exportButton->setExportColumns(singleton('AuditTrailDetail')->summaryFields());
	        
	        $listField = GridField::create(
	            'AuditTrailDetail',
	            false,
	            $this->AuditTrailDetails(),
	            $fieldConfig = GridFieldConfig_Base::create()
	                ->removeComponentsByType('GridFieldFilterHeader')
	                ->addComponents(new GridFieldButtonRow('after'), new GridFieldPrintButton('buttons-after-left'), $exportButton)
	        );
			
			$fields->removeByName('AuditTrailDetails');
			$fields->addFieldToTab('Root.Main', $listField);
        }

        return $fields;
    }
	
	function getUpdatedBy(){
        if($this->AuditTrailDetails()->sort('Created', 'DESC')->first()){
			$member = $this->AuditTrailDetails()->sort('Created', 'DESC')->first()->Member();
			return $member->exists() ? $member->Username : 'n/a';
		}
	}

	function getUpdatedIP(){
		return $this->AuditTrailDetails()->sort('Created', 'DESC')->first()->IPAddress;
	}

	function canView($member = false) {
        return true;
    }

    function canEdit($member = false) {
        return true;
    }

    function canDelete($member = false) {
        return false;
    }

    function canCreate($member = false) {
        return false;
    }
}

class AuditTrailDetail extends DataObject{
	private static $db = array(
		'OldValue' => 'Text',
		'NewValue' => 'Text',
		'IPAddress' => 'Varchar(100)'
	);
	
	private static $has_one = array(
		'Member' => 'Member',
		'AuditTrail' => 'AuditTrail'
	);
	
	private static $default_sort = "Created DESC, ID DESC";
	
	private static $searchable_fields = array(
    	'LastEdited' => array(
			'field' => 'DateField',
			'filter' => 'DateMatchFilter'
		),
		'Member.Username',
    	'IPAddress'
    );

    private static $summary_fields = array(
        'LastEdited.Nice',
        'OldData',
        'NewData',
        'Member.Username',
        'IPAddress'
    );
	
	public function fieldLabels($includerelations = true) {
		$labels = parent::fieldLabels($includerelations);
		
		$labels['LastEdited'] = _t('AuditTrailDetail.LAST_EDITED', 'Last Edited');
		$labels['LastEdited.Nice'] = _t('AuditTrailDetail.LAST_EDITED', 'Last Edited');
		$labels['OldData'] = _t('AuditTrailDetail.OLD_DATA', 'Old Data');
		$labels['NewData'] = _t('AuditTrailDetail.NEW_DATA', 'New Data');
		$labels['Member.Username'] = _t('AuditTrailDetail.UPDATED_BY', 'Updated By');
		$labels['IPAddress'] = _t('AuditTrailDetail.IP_ADDRESS', 'IP Address');
		
		return $labels;	
	}
    
    static function encode_data($data){
        $encode_data = base64_encode(serialize($data));
        return $encode_data;
    }
    
    static function decode_data($data){
        $decode_data = unserialize(base64_decode($data));
        return $decode_data;
    }
	
	function getOldData(){
		return var_export(AuditTrailDetail::decode_data($this->OldValue), true);
	}
	
	function getNewData(){
		return var_export(AuditTrailDetail::decode_data($this->NewValue), true);
	}
	
	function canView($member = false) {
        return true;
    }

    function canEdit($member = false) {
        return false;
    }

    function canDelete($member = false) {
        return false;
    }

    function canCreate($member = false) {
        return false;
    }
}
?>