<?php
class UpgradeAuditTrailSchema extends BuildTask {
    
    protected $title = 'Upgrade Audit Trail Schema';
    
    protected $description = 'Upgrade Audit Trail Schema';
    
    function init() {
        parent::init();
        $canAccess = (Director::isDev() || Director::is_cli() || Permission::check("ADMIN"));
        if(!$canAccess) return Security::permissionFailure($this);
    }
    
    public function run($request)
    {
        set_time_limit(0);
        $time = microtime();
        $time = explode(' ', $time);
        $time = $time[1] + $time[0];
        $start = $time;
        
		DB::alteration_message('Start upgrade audit trail schema', 'created');
		
		$data = array();

		DB::query("UPDATE AuditTrail a INNER JOIN AuditTrailDetail b on a.ID = b.AuditTrailID SET b.IPAddress = a.IPAddress, b.MemberID = a.MemberID");

		$audit_trails = DB::query("SELECT * FROM AuditTrail ORDER BY ID");
        foreach($audit_trails as $audit_trail){
        	$class = $audit_trail['ObjectClass'];
			$id = $audit_trail['RowID'];
        	if(isset($data[$class][$id])){
        		$data[$class][$id]['version'] += 1;
				DB::query(sprintf("UPDATE AuditTrailDetail set AuditTrailID = '%s' WHERE AuditTrailID = '%s'", $data[$class][$id]['id'], $audit_trail['ID']));
				DB::query(sprintf("UPDATE AuditTrail SET Version = '%s' WHERE ID = '%s'", $data[$class][$id]['version'], $data[$class][$id]['id']));
				DB::query(sprintf("DELETE FROM AuditTrail WHERE ID = '%s'", $audit_trail['ID']));
        	}
			else{
				$data[$class][$id]['id'] = $audit_trail['ID'];
				$data[$class][$id]['version'] = 1;
			}
        }
		
		DB::query('ALTER TABLE AuditTrail DROP IPAddress');
		DB::query('ALTER TABLE AuditTrail DROP MemberID');

		DB::alteration_message('Finish upgrade audit trail schema', 'created');
        
        $time = microtime();
        $time = explode(' ', $time);
        $time = $time[1] + $time[0];
        $finish = $time;
        $total_time = round(($finish - $start), 4);
        DB::alteration_message('Process Time - ' . $total_time . ' seconds', 'created');
    }
}

?>