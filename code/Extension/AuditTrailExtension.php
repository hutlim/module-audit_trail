<?php
class AuditTrailExtension extends DataExtension {
	function onAfterWrite(){
		if($this->owner->exists()){
			$data = $this->owner->getChangedFields(false, 1);
			if(sizeof($data)){
				$version = AuditTrail::get()->filter('ObjectClass', $this->owner->class)->filter('RowID', $this->owner->ID)->count() + 1;

				if(!$audit_trail = AuditTrail::get()->filter('ObjectClass', $this->owner->class)->find('RowID', $this->owner->ID)){
					$audit_trail = AuditTrail::create()->setField('ObjectClass', $this->owner->class)->setField('RowID', $this->owner->ID);
				}
				
				$audit_trail->Version += 1;
				$audit_trail->write();

                $before_data = array();
                $after_data = array();
				foreach($data as $field_name => $value){
				    $before_data[$field_name] = $value['before'];
                    $after_data[$field_name] = $value['after'];
				}
                
                $audit_trail_detail = Object::create('AuditTrailDetail');
                $audit_trail_detail->setField('OldValue', AuditTrailDetail::encode_data($before_data));
                $audit_trail_detail->setField('NewValue', AuditTrailDetail::encode_data($after_data));
                $audit_trail_detail->setField('AuditTrailID', $audit_trail->ID);
				$audit_trail_detail->setField('IPAddress', Controller::curr()->request->getIP());
				$audit_trail_detail->setField('MemberID', Member::currentUserID());
                $audit_trail_detail->write();
			}
		}
	}
}

?>